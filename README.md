# second_flask_website



## Name
second_flask_website

## Description
The second flask website made as fork of first flask website.
This is a proof of concept for the jinja templating engine 
using a navigation bar to switch between 3 different static 
sites.

## License
GPLv3

## Project status
Started
